package com.xuqiuyeli.todolistapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.xuqiuyeli.todolistapp.model.TodoList

class TodoListAdapter(
    private val allTodoLists: ArrayList<TodoList>,
    private val clickListener: TodoListClickListener
) : RecyclerView.Adapter<TodoListViewHolder>() {

    interface TodoListClickListener {
        fun todoListClicked(list: TodoList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoListViewHolder {
        val todoListView = LayoutInflater.from(parent.context)
            .inflate(R.layout.todolist_view_holder, parent, false)

        return TodoListViewHolder(todoListView)
    }

    override fun getItemCount(): Int {
        return allTodoLists.size
    }

    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
        holder.listPositionTextView.text = (position + 1).toString()
        holder.listTitleTextView.text = allTodoLists[position].name
        holder.itemView.setOnClickListener {
            clickListener.todoListClicked(allTodoLists[position])
        }
    }

    fun addList(list: TodoList) {
        allTodoLists.add(list)
        notifyItemInserted(allTodoLists.size -1)
    }
}