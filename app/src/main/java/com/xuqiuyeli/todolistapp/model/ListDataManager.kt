package com.xuqiuyeli.todolistapp.model

import android.content.Context
import androidx.preference.PreferenceManager

class ListDataManager(private val context: Context) {

    fun saveList(list: TodoList) {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context).edit()
        sharedPrefs.putStringSet(list.name, list.todoItems.toHashSet())
        sharedPrefs.apply()
    }

    fun readLists(): ArrayList<TodoList> {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val contents = sharedPrefs.all
        val allTodoLists = ArrayList<TodoList>()

        for(list in contents) {
            val todoItems = ArrayList(list.value as HashSet<String>)
            val todoList = TodoList(list.key, todoItems)
            allTodoLists.add(todoList)
        }

        return allTodoLists
    }
}