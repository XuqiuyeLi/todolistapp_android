package com.xuqiuyeli.todolistapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class TodoItemsAdapter(private val todoItems: ArrayList<String>) : RecyclerView.Adapter<TodoItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoItemViewHolder {
        val todoItemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.todoitem_view_holder, parent, false)

        return TodoItemViewHolder(todoItemView)
    }

    override fun getItemCount(): Int {
        return todoItems.size
    }

    override fun onBindViewHolder(holder: TodoItemViewHolder, position: Int) {
        holder.todoItemTitleTextView.text = todoItems[position]
    }
}
