package com.xuqiuyeli.todolistapp

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.xuqiuyeli.todolistapp.model.ListDataManager
import com.xuqiuyeli.todolistapp.model.TodoList

class TodoItemFragment : Fragment() {
    private lateinit var todoList: TodoList
    private lateinit var todoItemsRecyclerView: RecyclerView
    private lateinit var listDataManager: ListDataManager

    companion object {
        private const val ARG_LIST_NAME = "todo_list_name"
        private const val ARG_ITEMS = "todo_items"

        fun newInstance(list: TodoList) : TodoItemFragment {
            val bundle = Bundle()
            bundle.putString(ARG_LIST_NAME, list.name)
            bundle.putStringArrayList(ARG_ITEMS, list.todoItems)
            val fragment = TodoItemFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_todo_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            listDataManager = ListDataManager(it)
        }

        arguments?.let {
            val args = TodoItemFragmentArgs.fromBundle(it)
            todoList = listDataManager.readLists().filter { list -> list.name == args.listName }[0]
        }

        activity?.let {
            todoItemsRecyclerView = view.findViewById(R.id.todolist_items)
            todoItemsRecyclerView.layoutManager = LinearLayoutManager(it)
            todoItemsRecyclerView.adapter = TodoItemsAdapter(todoList.todoItems)
            it.title = todoList.name

            val addToDoItemButton = view.findViewById<FloatingActionButton>(R.id.addTodoItem)
            addToDoItemButton.setOnClickListener { _ ->
                showCreateItemDialog()
            }
        }
    }

    private fun addItem(newItem: String) {
        todoList.todoItems.add(newItem)
        listDataManager.saveList(todoList)
    }

    private fun showCreateItemDialog() {
        activity?.let {
            val dialogTitle = getString(R.string.name_of_task)
            val todoItemEditText = EditText(it)
            todoItemEditText.inputType = InputType.TYPE_CLASS_TEXT

            AlertDialog.Builder(it)
                .setTitle(dialogTitle)
                .setView(todoItemEditText)
                .setPositiveButton(R.string.create) { dialog, _ ->
                    val newItem = todoItemEditText.text.toString()
                    addItem(newItem)
                    dialog.dismiss()
                }
                .create()
                .show()
        }
    }
}