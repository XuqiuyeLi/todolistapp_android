package com.xuqiuyeli.todolistapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        Navigation.findNavController(this, R.id.nav_host_fragment_container)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        title = getString(R.string.app_name)
    }
}