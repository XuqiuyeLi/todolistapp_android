package com.xuqiuyeli.todolistapp

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.xuqiuyeli.todolistapp.model.ListDataManager
import com.xuqiuyeli.todolistapp.model.TodoList

class TodoListFragment : Fragment(), TodoListAdapter.TodoListClickListener {
    private lateinit var todoListRecyclerView: RecyclerView
    private lateinit var listDataManager: ListDataManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            listDataManager = ListDataManager(it)
        }

        val allLists = listDataManager.readLists()
        todoListRecyclerView = view.findViewById(R.id.todolist)
        todoListRecyclerView.layoutManager = LinearLayoutManager(activity)
        todoListRecyclerView.adapter = TodoListAdapter(allLists, this)

        val addToDoListButton = view.findViewById<FloatingActionButton>(R.id.addTodoList)
        addToDoListButton.setOnClickListener { _ ->
            showCreateTodoListDialog()
        }
    }


    private fun addList(newList: TodoList) {
        listDataManager.saveList(newList)
        val adapter = todoListRecyclerView.adapter as TodoListAdapter
        adapter.addList(newList)
    }


    private fun showCreateTodoListDialog() {
        activity?.let {
            val dialogTitle = getString(R.string.name_of_list)
            val todoListTitleEditText = EditText(it)
            todoListTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

            AlertDialog.Builder(it)
                .setTitle(dialogTitle)
                .setView(todoListTitleEditText)
                .setPositiveButton(R.string.create) { dialog, _ ->
                    val newList = TodoList(todoListTitleEditText.text.toString())
                    addList(newList)
                    dialog.dismiss()
                }
                .create()
                .show()
        }
    }

    override fun todoListClicked(list: TodoList) {
        view?.let {
            val action = TodoListFragmentDirections.actionTodoListFragmentToTodoItemFragment(listName = list.name)
            it.findNavController().navigate(action)
        }
    }
}